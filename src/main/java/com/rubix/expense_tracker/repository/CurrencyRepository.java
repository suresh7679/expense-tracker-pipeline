package com.rubix.expense_tracker.repository;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rubix.expense_tracker.model.Currency;


@Repository
public interface CurrencyRepository extends JpaRepository<Currency, Long> {
    
}
