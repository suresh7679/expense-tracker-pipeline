package com.rubix.expense_tracker.repository;





import java.util.Optional;


import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.rubix.expense_tracker.model.Users;



/**
 * @author  handles the repository 
 * The UserRepository  is used as a interface
 *  where JpaRepository is extended which helps to use CRUD operations 
 */
@Repository
public interface UserRepository extends JpaRepository<Users,Integer>{

	Optional<Users> findByEmail(String email);
	


	

}