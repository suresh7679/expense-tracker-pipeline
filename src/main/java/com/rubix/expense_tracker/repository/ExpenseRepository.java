 package com.rubix.expense_tracker.repository;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.rubix.expense_tracker.model.Expense;


@Repository
  public interface ExpenseRepository extends JpaRepository<Expense, Long>{
	@Query(value="SELECT * from Expense WHERE purchase_date between :x and :y",nativeQuery = true)
	public List<Expense> getByDate(@Param("x")Date start_date, @Param("y")Date end_date); 

	@Query(value="SELECT * from Expense  WHERE  username=:z  and  purchase_date between :x and :y ",nativeQuery = true)
	public List<Expense> getAll(@Param("x")Date start_date, @Param("y")Date end_date, @Param("z")String user); 
}
