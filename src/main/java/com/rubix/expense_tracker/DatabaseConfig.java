package com.rubix.expense_tracker;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/*
 * package com.rubix.expense_tracker; import com.zaxxer.hikari.HikariConfig;
 * import com.zaxxer.hikari.HikariDataSource; import
 * org.springframework.beans.factory.annotation.Value; import
 * org.springframework.context.annotation.Bean; import
 * org.springframework.context.annotation.Configuration; import
 * javax.sql.DataSource;
 * 
 * @Configuration public class DatabaseConfig {
 * 
 * @Value("${spring.datasource.url}") private String dbUrl;
 * 
 * @Bean public DataSource dataSource() { HikariConfig config = new
 * HikariConfig(); config.setJdbcUrl(dbUrl); return new
 * HikariDataSource(config); } }
 */


@EnableJpaRepositories(basePackages = "com.rubix.expense_tracker.resource")
@Configuration
public class DatabaseConfig {
	

}