package com.rubix.expense_tracker.service;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.rubix.expense_tracker.model.Users;
import com.rubix.expense_tracker.model.UserTypeModel;

public class UserExcelGenerator {
	
	public static ByteArrayInputStream usersToExcel(List<Users> users) throws IOException {
		String[] COLUMNs = {"S.No", "First Name", "Last Name", "Email ID","Mobile No","Status","User Type"};
		try(
				Workbook workbook = new XSSFWorkbook();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
		){
			CreationHelper createHelper = workbook.getCreationHelper();
	 
			Sheet sheet = workbook.createSheet("Users");
			sheet.setColumnWidth(0, 2000); 
			sheet.setColumnWidth(1, 7000);  
			sheet.setColumnWidth(2, 7000);  
			sheet.setColumnWidth(3, 7000);  
			sheet.setColumnWidth(4, 5000);  
			sheet.setColumnWidth(5, 3000);  
			sheet.setColumnWidth(6, 3000);
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
	 
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());  
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			headerCellStyle.setBorderBottom(BorderStyle.THIN);  
			headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			headerCellStyle.setBorderRight(BorderStyle.THIN);  
			headerCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			headerCellStyle.setBorderTop(BorderStyle.THIN);  
			headerCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
			
			//Style for Footer for summary
			Font footerFont = workbook.createFont();
			footerFont.setBold(true);
			footerFont.setColor(IndexedColors.BLACK.getIndex());
			
			CellStyle footerCellStyle = workbook.createCellStyle();
			footerCellStyle.setFont(footerFont);
			footerCellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			footerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	 
			// Row for Header
			Row headerRow = sheet.createRow(0);
	 
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
	 
			CellStyle ageCellStyle = workbook.createCellStyle();
			ageCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
			
			int rowIdx = 1;
			for (Users user : users) {
				Row row = sheet.createRow(rowIdx++);
	 
				row.createCell(0).setCellValue(user.getId());
				row.createCell(1).setCellValue(user.getFirstname());
				row.createCell(2).setCellValue(user.getLastname());
				row.createCell(3).setCellValue(user.getEmail());
				row.createCell(4).setCellValue(user.getMobile());
				row.createCell(5).setCellValue(user.getStatus());
				for(int i=0; i< user.getUsertype().size(); i++) {
		        	
		            row.createCell(6).setCellValue(user.getUsertype().get(i).getName());
				}
				
			}
			//footer for expense
			Row row2 = sheet.createRow(rowIdx);
			Cell fCell = row2.createCell(0);
			fCell.setCellStyle(footerCellStyle);
			Cell gCell = row2.createCell(1);
			gCell.setCellStyle(footerCellStyle);
			Cell hCell = row2.createCell(2);
			hCell.setCellStyle(footerCellStyle);
			Cell iCell = row2.createCell(3);
			iCell.setCellStyle(footerCellStyle);
			Cell jCell = row2.createCell(4);
			jCell.setCellStyle(footerCellStyle);
			Cell kCell = row2.createCell(5);
			kCell.setCellStyle(footerCellStyle);
			Cell lCell = row2.createCell(6);
			lCell.setCellStyle(footerCellStyle);
			
				
	 
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}
}
	
