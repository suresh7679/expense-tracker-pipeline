package com.rubix.expense_tracker.service;


import com.rubix.expense_tracker.model.*;
import com.rubix.expense_tracker.repository.*;
import java.sql.Date;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExpenseService {

    private ExpenseRepository expenseRepo;
    private CurrencyRepository currRepo;

    public ExpenseService( ExpenseRepository expenseRepo, CurrencyRepository currRepo) {
        this.expenseRepo = expenseRepo;
        this. currRepo =  currRepo;
    }
    /** Create a new User */
    public ResponseEntity<Object> createExpense(Expense model) {
    	Expense exp = new Expense();
    	
    	exp .setId(exp.getId());
    	
       String name=model.getName();
       if(name==null) {
    	   return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
       }
    	exp .setName(model.getName());
    	
    	
    	double quantity=model.getQuantity();
    	if(quantity==0) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setQuantity(model.getQuantity());
    	
    	
    	exp .setTax(model.getTax());
    	
    	
    	double price=model.getPrice();
    	if(price==0) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setPrice(model.getPrice());
    	
    	
    	double amount=model.getAmount();
    	if(amount==0) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setAmount(model.getAmount());
    	
    	
    	double total=model.getTotal();
    	if(total==0) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setTotal(model.getTotal());
    	
    	
    	Date date=model.getPurchase_date();
    	if(date==null) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setPurchase_date(model.getPurchase_date());
    	
    	
    	String username=model.getUsername();
    	if(username==null) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setUsername(model.getUsername());
    	
    	
    	List<Currency> currency=model.getCurrency();
    	if(currency==null) {
    		return ResponseEntity.unprocessableEntity().body("Failed to create the Expense details"); 
    	}
    	exp .setCurrency(model.getCurrency());

    	Expense savedExpense = expenseRepo.save(exp);
            if (expenseRepo.findById(savedExpense.getId()).isPresent()) {
            	long id=savedExpense.getId();
                return ResponseEntity.ok("Expense details Created Successfully with ID:"+id);}
            else return ResponseEntity.unprocessableEntity().body("Failed Creating Expense details");
        }
    

    /** Update an Existing User */
    @Transactional
    public ResponseEntity<Object> updateExpense(Expense exp, long  id) {
        if(expenseRepo.findById(id).isPresent()) {
        	Expense newExp = expenseRepo.findById(id).get();
        	
        	String name=exp.getName();
            if(name==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update the Expense details"); 
            }
        	newExp.setName(exp.getName());
        	
        	double quantity=exp.getQuantity();
        	if(quantity==0) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details"); 
        	}
        	newExp.setQuantity(exp.getQuantity());
        	
        	newExp.setTax(exp.getTax());
        	
        	double price=exp.getPrice();
        	if(price==0) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details ID:"+id); 
        	}
        	newExp.setPrice(exp.getPrice());
        	
        	double amount=exp.getAmount();
        	if(amount==0) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details ID:"+id); 
        	}
        	newExp.setAmount(exp.getAmount());
        	
        	double total=exp.getTotal();
        	if(total==0) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details ID:"+id); 
        	}
        	newExp.setTotal(exp.getTotal());
        	
        	Date date=exp.getPurchase_date();
        	if(date==null) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details ID:"+id); 
        	}
        	newExp.setPurchase_date(exp.getPurchase_date());
        	
        	String username=exp.getUsername();
        	if(username==null) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details ID:"+id); 
        	}
        	newExp.setUsername(exp.getUsername());
        	
        	
        	List<Currency> currency=exp.getCurrency();
        	if(currency==null) {
        		return ResponseEntity.unprocessableEntity().body("Failed to  update the Expense details ID:"+id); 
        	}
        	newExp.setCurrency(exp.getCurrency());
            Expense savedExpense = expenseRepo.save(newExp);
            if(expenseRepo.findById(savedExpense.getId()).isPresent()) {
            	long id1=savedExpense.getId();
                return  ResponseEntity.accepted().body("Expense details for ID:"+id1+" updated successfully");}
            else return ResponseEntity.unprocessableEntity().body("Failed updating the Expense_details of ID"+id);
        } else return ResponseEntity.unprocessableEntity().body("Cannot find the Expense details of ID:"+id);
    }
    /** Delete an User*/
    public ResponseEntity<Object> deleteExpense(long id) {
        if (expenseRepo.findById(id).isPresent()) {
        	expenseRepo.deleteById(id);
            if (expenseRepo.findById(id).isPresent())
                return ResponseEntity.unprocessableEntity().body("Failed to Delete the Expense details of ID:"+id);
            else return ResponseEntity.ok().body("Successfully deleted the  Expense details of ID:"+id);
        } else return ResponseEntity.badRequest().body("Cannot find the Expense details specified");
    }

    public ExpenseModel getExpense(long id) {
        if(expenseRepo.findById(id).isPresent()) {
            Expense exp = expenseRepo.findById(id).get();
            ExpenseModel expenseModel = new ExpenseModel();
            expenseModel.setId(exp.getId());
            expenseModel.setName(exp.getName());
            expenseModel.setQuantity(exp.getQuantity());
            expenseModel.setTax(exp.getTax());
            expenseModel.setPrice(exp.getPrice());
            expenseModel.setAmount(exp.getAmount());
            expenseModel.setTotal(exp.getTotal());
            expenseModel.setPurchase_date(exp.getPurchase_date());
            expenseModel.setUsername(exp.getUsername());
            expenseModel.setCurrency(getCurrencyList(exp));
            
            return expenseModel;
        } else return null;
    }
    public List<ExpenseModel > getExpenses() {
        List<Expense> expenseList = expenseRepo.findAll();
        if(expenseList.size()>0) {
            List<ExpenseModel> expenseModels = new ArrayList<>();
            for (Expense expense : expenseList) {
                ExpenseModel model = new ExpenseModel();
                model.setId(expense.getId());
                model.setName(expense.getName());
                model.setQuantity(expense.getQuantity());
                model.setTax(expense.getTax());
                model.setPrice(expense.getPrice());
                model.setAmount(expense.getAmount());
                model.setTotal(expense.getTotal());
                model.setPurchase_date(expense.getPurchase_date());
                model.setUsername(expense.getUsername());
               
                model.setCurrency(getCurrencyList(expense));
                expenseModels.add(model);
            }
            return expenseModels;
        } else return new ArrayList<ExpenseModel>();
    }
    private List<CurrencyModel> getCurrencyList(Expense exp){
        List<CurrencyModel> currList = new ArrayList<>();
        for(int i=0; i< exp.getCurrency().size(); i++) {
        	CurrencyModel currModel = new CurrencyModel();
            currModel.setCurrency(exp.getCurrency().get(i).getCurrency());
            currList.add(currModel);
        }
        return currList;
    }
}
