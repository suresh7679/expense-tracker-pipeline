package com.rubix.expense_tracker.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.rubix.expense_tracker.model.Expense;


public class ExpenseExcelGenerator {
	
	
	
	public static ByteArrayInputStream expenseToExcel(List<Expense> expenses) throws IOException {
		String[] COLUMNs = {"S.No", "Category Name", "Price", "Tax/GST","Amount","Total","Date of Purchase","Username","Currency"};
		
		String[] Colu= {"Username","Indian Expenses","US Expenses","Euro Expenses","Australian Expenses"};
		try(
				Workbook workbook = new XSSFWorkbook();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
		){
			CreationHelper createHelper = workbook.getCreationHelper();
	 
			Sheet sheet = workbook.createSheet("Expense");
			
			Sheet sheet2 = workbook.createSheet("Summary");
			
			sheet.setColumnWidth(0, 2000); 
			sheet.setColumnWidth(1, 5000);  
			sheet.setColumnWidth(2, 4000);  
			sheet.setColumnWidth(3, 4000);  
			sheet.setColumnWidth(4, 4000);  
			sheet.setColumnWidth(5, 4000);  
			sheet.setColumnWidth(6, 5000);  
			sheet.setColumnWidth(7, 4000);
			sheet.setColumnWidth(8, 5000);
			sheet.setColumnWidth(9, 5000); 
			sheet.setColumnWidth(10, 5000); 
			
			
			
			sheet2.setColumnWidth(0, 5000); 
			sheet2.setColumnWidth(1, 5000);  
			sheet2.setColumnWidth(2, 5000);
			sheet2.setColumnWidth(3, 5000);
			sheet2.setColumnWidth(4, 7000);
			
			//Styles for Header
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setColor(IndexedColors.BLACK.getIndex());
			headerFont.setItalic(true); 
			headerFont.setFontName("Arial");  
			
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			headerCellStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());  
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			headerCellStyle.setBorderBottom(BorderStyle.THICK);  
			headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			headerCellStyle.setBorderRight(BorderStyle.THICK);  
			headerCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			headerCellStyle.setBorderTop(BorderStyle.THICK);  
			headerCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
			
			
			//Style for Footer for summary
			Font footerFont = workbook.createFont();
			footerFont.setBold(true);
			footerFont.setColor(IndexedColors.BLACK.getIndex());
			
			CellStyle footerCellStyle = workbook.createCellStyle();
			footerCellStyle.setFont(footerFont);
			footerCellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			footerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			
			CellStyle footerCellStyle1 = workbook.createCellStyle();
			footerCellStyle1.setFont(footerFont);
			footerCellStyle1.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			footerCellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			footerCellStyle1.setBorderBottom(BorderStyle.THICK);  
			footerCellStyle1.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			footerCellStyle1.setBorderRight(BorderStyle.THICK);  
			footerCellStyle1.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			footerCellStyle1.setBorderTop(BorderStyle.THICK);  
			footerCellStyle1.setTopBorderColor(IndexedColors.BLACK.getIndex());
			
			// Row for Header
			Row headerRow = sheet.createRow(0);
			
			Row headerRow2 = sheet2.createRow(0);
	 
			// Header
			for (int col = 0; col < COLUMNs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(COLUMNs[col]);
				cell.setCellStyle(headerCellStyle);
			}
			
			for (int col = 0; col < Colu.length; col++) {
				Cell cell = headerRow2.createCell(col);
				cell.setCellValue(Colu[col]);
				cell.setCellStyle(headerCellStyle);
			}
			
			
			//style for date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/mm/yyyy"));
			
			
			
			//style for inr
			CellStyle inrCellStyle = workbook.createCellStyle();
			inrCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("₹#,##0.00"));
			
			CellStyle inrCellStyle1 = workbook.createCellStyle();
			inrCellStyle1.setDataFormat(createHelper.createDataFormat().getFormat("₹#,##0.00"));
			inrCellStyle1.setFont(footerFont);
			inrCellStyle1.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			inrCellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			inrCellStyle1.setBorderBottom(BorderStyle.THICK);  
			inrCellStyle1.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			inrCellStyle1.setBorderRight(BorderStyle.THICK);  
			inrCellStyle1.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			inrCellStyle1.setBorderTop(BorderStyle.THICK);  
			inrCellStyle1.setTopBorderColor(IndexedColors.BLACK.getIndex());
			
			//style for usd
			CellStyle usdCellStyle = workbook.createCellStyle();
			usdCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("$#,##0.00"));
			
			CellStyle usdCellStyle1 = workbook.createCellStyle();
			usdCellStyle1.setDataFormat(createHelper.createDataFormat().getFormat("$#,##0.00"));
			usdCellStyle1.setFont(footerFont);
			usdCellStyle1.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			usdCellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			usdCellStyle1.setBorderBottom(BorderStyle.THICK);  
			usdCellStyle1.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			usdCellStyle1.setBorderRight(BorderStyle.THICK);  
			usdCellStyle1.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			usdCellStyle1.setBorderTop(BorderStyle.THICK);  
			usdCellStyle1.setTopBorderColor(IndexedColors.BLACK.getIndex());
			
			//style for eur
			CellStyle eurCellStyle = workbook.createCellStyle();
			eurCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("€#,##0.00"));
			
			CellStyle eurCellStyle1 = workbook.createCellStyle();
			eurCellStyle1.setDataFormat(createHelper.createDataFormat().getFormat("€#,##0.00"));
			eurCellStyle1.setFont(footerFont);
			eurCellStyle1.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			eurCellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			eurCellStyle1.setBorderBottom(BorderStyle.THICK);  
			eurCellStyle1.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			eurCellStyle1.setBorderRight(BorderStyle.THICK);  
			eurCellStyle1.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			eurCellStyle1.setBorderTop(BorderStyle.THICK);  
			eurCellStyle1.setTopBorderColor(IndexedColors.BLACK.getIndex());
			
			//style for aud
			CellStyle audCellStyle = workbook.createCellStyle();
			audCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("A$#,##0.00"));
			
			CellStyle audCellStyle1 = workbook.createCellStyle();
			audCellStyle1.setDataFormat(createHelper.createDataFormat().getFormat("A$#,##0.00"));
			audCellStyle1.setFont(footerFont);
			audCellStyle1.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());  
			audCellStyle1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			audCellStyle1.setBorderBottom(BorderStyle.THICK);  
			audCellStyle1.setBottomBorderColor(IndexedColors.BLACK.getIndex());  
			audCellStyle1.setBorderRight(BorderStyle.THICK);  
			audCellStyle1.setRightBorderColor(IndexedColors.BLACK.getIndex());  
			audCellStyle1.setBorderTop(BorderStyle.THICK);  
			audCellStyle1.setTopBorderColor(IndexedColors.BLACK.getIndex());
			
			
		
			int rowIdx = 1;
			for (Expense exp : expenses) {
				Row row = sheet.createRow(rowIdx++);
				
				row.createCell(0).setCellValue(exp.getId());
			
				row.createCell(1).setCellValue(exp.getName());
				
				Cell aCell = row.createCell(2);
				aCell.setCellValue(exp.getPrice());
				
				
				Cell bCell = row.createCell(3);
				double price=exp.getPrice();
				double tax=exp.getTax();
				double tax1=price*(tax/100);
				bCell.setCellValue(tax1);
				
				
				Cell cCell = row.createCell(4);
				cCell.setCellValue(exp.getAmount());
				
				Cell dCell = row.createCell(5);
				dCell.setCellValue(exp.getTotal());
				
				
				Cell eCell = row.createCell(6);
				eCell.setCellValue(exp.getPurchase_date());
				eCell.setCellStyle(dateCellStyle);
				
				
				
				row.createCell(7).setCellValue(exp.getUsername());
				
				for(int i=0, b=8; i< exp.getCurrency().size(); i++,b++) {
		            row.createCell(b).setCellValue(exp.getCurrency().get(i).getCurrency().getCurrency());
		            
				}
				for(int i=0; i< exp.getCurrency().size(); i++) {
				String temp=exp.getCurrency().get(i).getCurrency().getCurrency();
				
	            if(temp.equals("Indian Rupees")) {
	            	aCell.setCellStyle(inrCellStyle);
	            	bCell.setCellStyle(inrCellStyle);
	            	cCell.setCellStyle(inrCellStyle);
	            	dCell.setCellStyle(inrCellStyle);
	            	
	            	
	            }
	            else if(temp.equals("US Dollars")) {
	            	aCell.setCellStyle(usdCellStyle);
	            	bCell.setCellStyle(usdCellStyle);
	            	cCell.setCellStyle(usdCellStyle);
	            	dCell.setCellStyle(usdCellStyle);
	            	}
	            else  if(temp.equals("Euro")) {
	            	aCell.setCellStyle(eurCellStyle);
	            	bCell.setCellStyle(eurCellStyle);
	            	cCell.setCellStyle(eurCellStyle);
	            	dCell.setCellStyle(eurCellStyle);
	            	
	            }
	            else  if(temp.equals("Australian Dollars")) {
	            	aCell.setCellStyle(audCellStyle);
	            	bCell.setCellStyle(audCellStyle);
	            	cCell.setCellStyle(audCellStyle);
	            	dCell.setCellStyle(audCellStyle);
	            	
	            }
	            else {
	            	aCell.setCellStyle(inrCellStyle);
	            	bCell.setCellStyle(inrCellStyle);
	            	cCell.setCellStyle(inrCellStyle);
	            	dCell.setCellStyle(inrCellStyle);
	            	
	            }
				}
				
	}
			int rowIdxi = 1;
			for (Expense exp : expenses) {
				Row row = sheet2.createRow(rowIdxi++);
				row.createCell(0).setCellValue(exp.getUsername());
				
				Cell aCell = row.createCell(1);
				aCell.setCellStyle(inrCellStyle);

				Cell bCell = row.createCell(2);
				bCell.setCellStyle(usdCellStyle);
				
				Cell cCell = row.createCell(3);
				cCell.setCellStyle(eurCellStyle);
				
				Cell dCell = row.createCell(4);
				dCell.setCellStyle(audCellStyle);
				 
				for(int i=0; i< exp.getCurrency().size(); i++) {
					String temp=exp.getCurrency().get(i).getCurrency().getCurrency();
					  if(temp.equals("Indian Rupees")) {
						  aCell.setCellValue(exp.getTotal()); 
						  bCell.setCellValue(0.00);
						  cCell.setCellValue(0.00);
						  dCell.setCellValue(0.00);
						  
					  }
					  else  if(temp.equals("US Dollars")) {
						  aCell.setCellValue(0.00);
						  bCell.setCellValue(exp.getTotal()); 
						  cCell.setCellValue(0.00);
						  dCell.setCellValue(0.00);
					  }
					  else  if(temp.equals("Euro")) {
						  aCell.setCellValue(0.00);
						  bCell.setCellValue(0.00);
						  cCell.setCellValue(exp.getTotal());
						  dCell.setCellValue(0.00);
					  }
					  else  if(temp.equals("Australian Dollars")) {
						  aCell.setCellValue(0.00);
						  bCell.setCellValue(0.00);
						  cCell.setCellValue(0.00);
						  dCell.setCellValue(exp.getTotal()); 
					  }
					  
						  
				}
			}
			
			//Total
			int num=rowIdxi;
			
			Row row1 = sheet2.createRow(rowIdxi);
			Cell aCell = row1.createCell(0);
			aCell.setCellValue("Total");
			aCell.setCellStyle(footerCellStyle1);
			
			// Create SUM formula
			Cell bCell = row1.createCell(1);
			bCell.setCellFormula("SUM(B2:B"+num+")");
			bCell.setCellStyle(inrCellStyle1);
			
			
			Cell cCell = row1.createCell(2);
			cCell.setCellFormula("SUM(C2:C"+num+")");
			cCell.setCellStyle(usdCellStyle1);
			
			
			Cell dCell = row1.createCell(3);
			dCell.setCellFormula("SUM(D2:D"+num+")");
			dCell.setCellStyle(eurCellStyle1);
			
			
			Cell eCell = row1.createCell(4);
			eCell.setCellFormula("SUM(E2:E"+num+")");
			eCell.setCellStyle(audCellStyle1);
			
			//footer for expense
			Row row2 = sheet.createRow(rowIdx);
			Cell fCell = row2.createCell(0);
			fCell.setCellStyle(footerCellStyle);
			Cell gCell = row2.createCell(1);
			gCell.setCellStyle(footerCellStyle);
			Cell hCell = row2.createCell(2);
			hCell.setCellStyle(footerCellStyle);
			Cell iCell = row2.createCell(3);
			iCell.setCellStyle(footerCellStyle);
			Cell jCell = row2.createCell(4);
			jCell.setCellStyle(footerCellStyle);
			Cell kCell = row2.createCell(5);
			kCell.setCellStyle(footerCellStyle);
			Cell lCell = row2.createCell(6);
			lCell.setCellStyle(footerCellStyle);
			Cell mCell = row2.createCell(7);
			mCell.setCellStyle(footerCellStyle);
			Cell nCell = row2.createCell(8);
			nCell.setCellStyle(footerCellStyle);
			
			
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		}
	}
}
	
