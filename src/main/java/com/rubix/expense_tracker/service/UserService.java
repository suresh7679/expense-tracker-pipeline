package com.rubix.expense_tracker.service;


import com.rubix.expense_tracker.model.*;
import com.rubix.expense_tracker.repository.*;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserService {

    private UserRepository userRepo;
    private UserTypeRepository typerepo;

    public UserService(UserRepository userRepo,  UserTypeRepository typerepo) {
        this.userRepo = userRepo;
        this.typerepo = typerepo;
    }
    /** Create a new User */
    public ResponseEntity<Object> createUser(Users model) {
        Users user = new Users();
        if (userRepo.findByEmail(model.getEmail()).isPresent()) {
            return ResponseEntity.badRequest().body("The Email is already Present, Failed to Create new User");
        } else {
        	 user.setId(user.getId());
        	 String fname=model.getFirstname();
             if(fname==null) {
          	   return ResponseEntity.unprocessableEntity().body("Failed to create new User"); 
             }
            user.setFirstname(model.getFirstname());
            
            String lname=model.getLastname();
            if(lname==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to create new User"); 
            }
            user.setLastname(model.getLastname());
            
            String mobile=model.getMobile();
            if(mobile==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to create new User"); 
            }
            user.setMobile(model.getMobile());
            
            String email=model.getEmail();
            if(email==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to create new User"); 
            }
            user.setEmail(model.getEmail());
            
            String status=model.getStatus();
            if(status==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to create new User"); 
            }
            user.setStatus(model.getStatus());
            
            List<UserType> type=model.getUsertype();
        	if(type==null) {
        		return ResponseEntity.unprocessableEntity().body("Failed to create new User"); 
        	}
            user.setUsertype(model.getUsertype());
           
            Users savedUser = userRepo.save(user);
            
            if (userRepo.findById(savedUser.getId()).isPresent()) {
            	long id=savedUser.getId();
                return ResponseEntity.ok("User Created Successfully with ID"+id);}
            
            else return ResponseEntity.unprocessableEntity().body("Failed Creating User");
        }
    }

    /** Update an Existing User */
    @Transactional
    public ResponseEntity<Object> updateUser(Users user, int  id) {
        if(userRepo.findById(id).isPresent()) {
            Users newUser = userRepo.findById(id).get();
            String  fname=user.getFirstname();
            if(fname==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update User"); 
            }
            newUser.setFirstname(user.getFirstname());
            
            String  lname=user.getLastname();
            if(lname==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update User"); 
            }
            newUser.setLastname(user.getLastname());
            
            String  mobile=user.getMobile();
            if(mobile==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update User"); 
            }
            newUser.setMobile(user.getMobile());
            
            String  email=user.getEmail();
            if(email==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update User"); 
            }
            newUser.setEmail(user.getEmail());
            
            String status=user.getStatus();
            if(status==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update User"); 
            }
            newUser.setStatus(user.getStatus());
            
            List<UserType> type=user.getUsertype();
            if(type==null) {
         	   return ResponseEntity.unprocessableEntity().body("Failed to update User"); 
            }
            newUser.setUsertype(user.getUsertype());
            Users savedUser = userRepo.save(newUser);
           
            if(userRepo.findById(savedUser.getId()).isPresent())
                return  ResponseEntity.accepted().body("User updated successfully for ID:"+id);
            else return ResponseEntity.unprocessableEntity().body("Failed updating the user for ID"+id);
        } else return ResponseEntity.unprocessableEntity().body("Cannot find the user");
    }
    /** Delete an User*/
    public ResponseEntity<Object> deleteUser(int id) {
        if (userRepo.findById(id).isPresent()) {
        	userRepo.deleteById(id);
            if (userRepo.findById(id).isPresent())
                return ResponseEntity.unprocessableEntity().body("Failed to Delete the  User for ID:"+id);
            else return ResponseEntity.ok().body("Successfully deleted the  User for ID:"+id);
        } else return ResponseEntity.badRequest().body("Cannot find the user for ID:"+id);
    }

    public UserModel getUser(int id) {
        if(userRepo.findById(id).isPresent()) {
            Users user = userRepo.findById(id).get();
            UserModel userModel = new UserModel();
            userModel.setId(user.getId());
            userModel.setFirstname(user.getFirstname());
            userModel.setLastname(user.getLastname());
            userModel.setEmail(user.getEmail());
            userModel.setMobile(user.getMobile());
            userModel.setStatus(user.getStatus());
            userModel.setUsertype(getUsertypeList(user));
            
            return userModel;
        } else return null;
    }
    public List<UserModel > getUsers() {
        List<Users> userList = userRepo.findAll();
        if(userList.size()>0) {
            List<UserModel> userModels = new ArrayList<>();
            for (Users user : userList) {
                UserModel model = new UserModel();
                model.setId(user.getId());
                model.setFirstname(user.getFirstname());
                model.setLastname(user.getLastname());
                model.setMobile(user.getMobile());
                model.setEmail(user.getEmail());
                model.setStatus(user.getStatus());
                model.setUsertype(getUsertypeList(user));
                userModels.add(model);
            }
            return userModels;
        } else return new ArrayList<UserModel>();
    }
    private List<UserTypeModel> getUsertypeList(Users user){
        List<UserTypeModel> typeList = new ArrayList<>();
        for(int i=0; i< user.getUsertype().size(); i++) {
        	UserTypeModel typeModel = new UserTypeModel();
            typeModel.setName(user.getUsertype().get(i).getName());
           
            typeList.add(typeModel);
        }
        return typeList;
    }
}







