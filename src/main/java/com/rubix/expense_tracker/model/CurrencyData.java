package com.rubix.expense_tracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

@Table(name = "currencydata",catalog="test",schema="public")
public class CurrencyData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "currency", nullable = false)
	private String currency;
	@Column(name = "code", nullable = false)
	private String code;
	
	
	public CurrencyData() {
		
	}
	public CurrencyData(long id, String currency, String code) {
		super();
		this.id = id;
		this.currency = currency;
		this.code = code;
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return "CurrencyData [id=" + id + ", currency=" + currency +  ",code="+code+"]";
	}

}
