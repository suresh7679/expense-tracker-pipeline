package com.rubix.expense_tracker.model;

import java.util.List;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")

@Entity

@Table(name = "currency",catalog="test",schema="public")

public class Currency {
	public Currency() {

	}
	

	public Currency(long id, Currencies currency, String code) {
		super();
		this.id = id;
		this.currency = currency;
		
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	 @Embedded
	private Currencies currency;
	

	@ManyToMany(targetEntity = Expense.class, mappedBy = "currency", cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH})

	private List<Expense> expense;


	

	

	
	/**
	 * @return the expense
	 */
	

	public long getId() {
		return id;
	}

	

	/**
	 * @return the currency
	 */
	public Currencies getCurrency() {
		return currency;
	}


	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(Currencies currency) {
		this.currency = currency;
	}


	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the expense
	 */
	

	

	/**
	 * @return the expense
	 */
	public List<Expense> getExpense() {
		return expense;
	}

	/**
	 * @param expense the expense to set
	 */
	public void setExpense(List<Expense> expense) {
		this.expense = expense;
	}

	

	/**
	 * @return the userIden
	 */
	@Override
	public String toString() {
		return "Currency [id=" + id + ", currency=" + currency +  "]";
	}

}
