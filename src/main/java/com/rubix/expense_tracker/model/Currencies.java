package com.rubix.expense_tracker.model;


import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Embeddable
public class Currencies {
	@NotNull
    @Size(max = 100)
	private String code;
	@NotNull
    @Size(max = 100)
	private String currency;
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Currencies() {
		
	}
	public Currencies(@NotNull @Size(max = 100) String code, @NotNull @Size(max = 100) String currency) {
		super();
		this.code = code;
		this.currency = currency;
	}
	
	
}
