package com.rubix.expense_tracker.model;

import java.sql.Date;
import java.util.List;

public class ExpenseModel {
	private long id;
	private String name;
    private long quantity;
	private double tax;
	private double price;
	private double amount;
	private double total;
	private Date purchase_date;
	private String username;
	
	private List < CurrencyModel > currency;
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	/**
	 * @return the purchase_date
	 */
	public Date getPurchase_date() {
		return purchase_date;
	}
	/**
	 * @param purchase_date the purchase_date to set
	 */
	public void setPurchase_date(Date purchase_date) {
		this.purchase_date = purchase_date;
	}
	
	
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the currency
	 */
	public List<CurrencyModel> getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(List<CurrencyModel> currency) {
		this.currency = currency;
	}
	
	

}
