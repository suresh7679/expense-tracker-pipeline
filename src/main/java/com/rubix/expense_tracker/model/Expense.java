package com.rubix.expense_tracker.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
@Entity

@Table(name = "expense" ,   catalog="test",schema="public")
public class Expense {
	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
    private long quantity;
	private double tax;
	private double price;
	private double amount;
	private double total;
	private Date purchase_date;
	private String username;
	
	

	@ManyToMany(targetEntity = Currency.class, cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH} )
		   private List <Currency> currency;
	 
	
	

	public Expense() {
		
	}
	
	

	
	public Expense(long id, String name, long quantity, double tax, double price, double amount, double total,
			Date purchase_date, String username) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.tax = tax;
		this.price = price;
		this.amount = amount;
		this.total = total;
		this.purchase_date = purchase_date;
		this.username = username;
	
	}




	public long getId() {
		return id;
	}




	public void setId(long id) {
		this.id = id;
	}



	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}



	@Column(name = "quantity", nullable = false)
	public long getQuantity() {
		return quantity;
	}




	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}



	@Column(name = "tax", nullable = false)
	public double getTax() {
		return tax;
	}




	public void setTax(double tax) {
		this.tax = tax;
	}



	@Column(name = "price", nullable = false)
	public double getPrice() {
		return price;
	}




	public void setPrice(double price) {
		this.price = price;
	}



	@Column(name = "amount", nullable = false)
	public double getAmount() {
		return amount;
	}




	public void setAmount(double amount) {
		this.amount = amount;
	}



	@Column(name = "total", nullable = false)
	public double getTotal() {
		return total;
	}




	public void setTotal(double total) {
		this.total = total;
	}


	@Column(name = "purchase_date", nullable = false)
	public Date getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(Date purchase_date) {
		this.purchase_date = purchase_date;
	}
	

	

	/**
	 * @return the username
	 */
	@Column(name = "username", nullable = false)
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the currency
	 */
	public List<Currency> getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(List<Currency> currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Expense [id=" + id + ",name =" + name+ ",quantity=" + quantity + ",tax=" +tax + ",price= " + price + ",amount =" +amount + ", total = "+ total + ",purchase_date =" + purchase_date +",username="+username+"]";
	}
	
}