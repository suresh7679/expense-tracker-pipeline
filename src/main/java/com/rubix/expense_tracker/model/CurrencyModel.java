package com.rubix.expense_tracker.model;



public class CurrencyModel {
	private Currencies currency;

	/**
	 * @return the currency
	 */
	public Currencies getCurrency() {
		return currency;
	}

	/**
	 * @param list the currency to set
	 */
	public void setCurrency(Currencies list) {
		this.currency = list;
	}
	
}
