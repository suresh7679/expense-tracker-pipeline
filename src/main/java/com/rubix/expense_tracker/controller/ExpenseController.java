package com.rubix.expense_tracker.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.Date;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


import com.rubix.expense_tracker.model.Expense;
import com.rubix.expense_tracker.model.ExpenseModel;

import com.rubix.expense_tracker.model.CurrencyData;
import com.rubix.expense_tracker.repository.CurrencyDataRepository;
import com.rubix.expense_tracker.repository.ExpenseRepository;

import com.rubix.expense_tracker.service.ExpenseService;

import com.rubix.expense_tracker.service.ExpenseExcelGenerator;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class ExpenseController {
	@Autowired
	private CurrencyDataRepository currRep;
	private ExpenseRepository expenseRepository;
	private ExpenseService expService;
	
	public ExpenseController( ExpenseService expService, ExpenseRepository expenseRepository) {
        this. expService =  expService;
        this.expenseRepository = expenseRepository;
    }
	
	@PostMapping("/expense")
    public ResponseEntity<Object> createUser(@RequestBody Expense exp) {
        return expService.createExpense(exp);
    }
    @GetMapping("/expense/{id}")
    public ExpenseModel getExpense(@PathVariable long id) {
        return expService.getExpense(id);
    	 
    }
  
    @GetMapping("/expense/{start_date}/{end_date}")
    public List<Expense> getExpenses(@PathVariable(value = "start_date")@DateTimeFormat(iso = ISO.DATE)Date start_date, 
    		@PathVariable(value = "end_date")@DateTimeFormat(iso = ISO.DATE) Date end_date ) {
        
    	 return expenseRepository.getByDate(start_date, end_date);
    	
    }
    
    
    @PutMapping("/expense/{id}")
    public ResponseEntity<Object> updateExpense(@PathVariable long id, @RequestBody Expense exp) {
        return expService.updateExpense(exp, id);
    }
    @DeleteMapping("expense/{id}")
    public ResponseEntity<Object> deleteExpense(@PathVariable long  id) {
        return expService.deleteExpense(id);
    }
	
	
    @GetMapping("/currencydata")
    public List<CurrencyData> getCurrency() {
        return currRep.findAll();

    }
	
	
    @GetMapping("/ByDate/{start_date}/{end_date}")
    public ResponseEntity<InputStreamResource> excelReport(
    		@PathVariable(value = "start_date")@DateTimeFormat(iso = ISO.DATE)Date start_date, 
    		@PathVariable(value = "end_date")@DateTimeFormat(iso = ISO.DATE) Date end_date) throws IOException {
    	
    		
    	List<Expense> expen = (List<Expense>) expenseRepository.getByDate(start_date, end_date);
    		 ByteArrayInputStream in  = ExpenseExcelGenerator.expenseToExcel(expen);
    		 
    	
    	
     	HttpHeaders headers = new HttpHeaders();
         headers.add("Content-Disposition", "attachment; filename=expense.xlsx");
		
    	 return ResponseEntity
	                .ok()
	                .headers(headers)
	                .body(new InputStreamResource(in));
	
		
		
    }
    
	
    @GetMapping("/ByDate/{start_date}/{end_date}/{user}")
    public ResponseEntity<InputStreamResource> excelReportU(
    		@PathVariable(value = "start_date")@DateTimeFormat(iso = ISO.DATE)Date start_date, 
    		@PathVariable(value = "end_date")@DateTimeFormat(iso = ISO.DATE) Date end_date, @PathVariable(value = "user")String user) throws IOException {
    	
    		
    	List<Expense> expen = (List<Expense>) expenseRepository.getAll(start_date, end_date,user);
    		 ByteArrayInputStream in  = ExpenseExcelGenerator.expenseToExcel(expen);
    		 
    	
    	
     	HttpHeaders headers = new HttpHeaders();
         headers.add("Content-Disposition", "attachment; filename=expense.xlsx");
		
    	 return ResponseEntity
	                .ok()
	                .headers(headers)
	                .body(new InputStreamResource(in));
	
		
		
    }
	
	
}