package com.rubix.expense_tracker.controller;



import java.io.ByteArrayInputStream;
import java.io.IOException;

import java.util.List;

//import java.util.Set;
//import java.util.Set;

//import javax.validation.Valid;


import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.rubix.expense_tracker.service.UserExcelGenerator;
import com.rubix.expense_tracker.model.Users;

import com.rubix.expense_tracker.model.UserModel;

import com.rubix.expense_tracker.repository.UserRepository;

import com.rubix.expense_tracker.service.UserService;





/**
 * @author Rubix
 * The UserController throws the response whenever the service is called from frontEnd
 * @CrossOrigin is used to link with the frontEnd URL
 *  @RequestMapping is used to map the backEnd to the frontEnd
 */


@RestController
@CrossOrigin(origins="http://localhost:4200")
@RequestMapping("/api/v1")
public class UserController  {
	
	private UserService userService;
    private UserRepository userRepository;
    
    public UserController(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }
   
    @PostMapping("/user")
    public ResponseEntity<Object> createUser(@RequestBody Users user) {
        return userService.createUser(user);
    }
    @GetMapping("/user/{id}")
    public UserModel getUser(@PathVariable int id) {
        return userService.getUser(id);
    }
    @GetMapping("/user")
    public List<UserModel> getUser() {
        return userService.getUsers();

    }
    @PutMapping("/user/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable int id, @RequestBody Users user) {
        return userService.updateUser(user, id);
    }
    @DeleteMapping("user/{id}")
    public ResponseEntity<Object> deleteUser(@PathVariable int  id) {
        return userService.deleteUser(id);
    }
    
    @GetMapping("/user/users.xlsx")
    public ResponseEntity<InputStreamResource> excelReport() throws IOException {
        List<Users> users = (List<Users>) userRepository.findAll();
		
		ByteArrayInputStream in = UserExcelGenerator.usersToExcel(users);
		
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=users.xlsx");
		
		 return ResponseEntity
	                .ok()
	                .headers(headers)
	                .body(new InputStreamResource(in));
    }
    
}